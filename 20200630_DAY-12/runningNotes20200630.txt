Agenda :

Database :

1. RDS
2. DynamoDB
3. ElastiCache
4. Neptune
5. Amazon Redshift
6. Amazon QLDB
7. Amazon DocumentDB
8. Amazon Keyspaces


Connection Tools:

https://dbeaver.io/

https://dev.mysql.com/downloads/workbench/

Endpoint
cloudbinary.cwwmiis4pqnh.us-east-1.rds.amazonaws.com

Port
3306

Master username
admin

Master password
redhat1234

select version()

create database python;

create table python.students(firstname varchar(30),lastname,emailid,mobilenumber,course,fee)

SQL Commands :

    - DDL : create , alter , drop , truncate, rename 
    - DML : insert , update , delete 
    - DQL / DRL : select
    - TCL : commit , rollback , savepoint 
    - DCL : grant , revoke 
    