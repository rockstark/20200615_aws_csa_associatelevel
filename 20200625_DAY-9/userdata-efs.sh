#!/bin/bash 
# Our OS is Ubutnu and Update the Local Repository
sudo apt-get update 

# Install Utility Softwares
sudo apt-get install vim curl wget unzip elinks tree git -y 

# Download, Install & Configure WebServer i.e. Apache - apache2 
sudo apt-get install apache2 -y 

# Enable the Daemon / Service at Boot Level 
sudo systemctl enable apache2.service 

# Start the Daemon / Service 
sudo systemctl start apache2.service 

# Delete default index.html 
sudo rm -rf /var/www/html/index.html 

# Install EFS Agent
sudo apt-get install nfs-common -y 

# Mount EFS File System on DocumentRoot i.e. /var/www/html 
sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport fs-cca2e74f.efs.us-east-1.amazonaws.com:/ /var/www/html/

# Udpate the DocumentRoot using echo command 
echo "`hostname`" >> /var/www/html/index.html 
echo "`hostname -I`" >> /var/www/html/index.html 
echo "Welcome to AWS Elastic File System" >> /var/www/html/index.html 

# Restart the Daemon / Service 
sudo systemctl restart apache2.service